/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugin.dev;

import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IContextPluginConfigurationViewFactory;
import org.ambientdynamix.contextplugin.dev.TestConfigurationHostActivity;
import org.ambientdynamix.contextplugin.dev.TestHarness;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

/**
 * An Android Activity configured to host a dynamically injected a configuration user interface provided by an
 * authorized ContextPluginRuntime.
 * 
 * @author Darren Carlson
 */
public class TestConfigurationHostActivity extends Activity {
    private final String TAG = this.getClass().getSimpleName();
    public static int TITLEBAR_HEIGHT;
    // Private data
    private IContextPluginConfigurationViewFactory viewFactory;
    private final Handler handler = new Handler();

    @Override
    public void onResume() {
	Log.v(TAG, "Activity State: onResume()");
	super.onResume();
	if (viewFactory != null) {
	    try {
		viewFactory.destroyView();
	    }
	    catch (Exception e) {
		Log.w(TAG, "Exception when destroying view: " + e);
	    }
	    viewFactory = null;
	    setContentView(null);
	}

	// Grab the intent and the pluginId
	Intent i = getIntent();
	Bundle extras = i.getExtras();
	String pluginId = extras.getString("pluginId");
	if (pluginId != null) {
	    Log.i(TAG, "getContextPluginRuntime for pluginId: " + pluginId);
	    /*
	     * Use the TestHarness to grab the plug-in's runtime.
	     */
	    final ContextPluginRuntime runtime = TestHarness.getContextPluginRuntime();
	    if (runtime != null) {
		Log.i(TAG, "getContextPluginRuntime result was: " + runtime);
		try {
		    Class<IContextPluginConfigurationViewFactory> factory = runtime.getSettingsViewFactory();
		    if (factory != null) {
			viewFactory = (IContextPluginConfigurationViewFactory) factory.newInstance();
			final View v = viewFactory.initializeView(
				runtime.getPluginFacade().getSecuredContext(runtime.getSessionId()), runtime,
				TITLEBAR_HEIGHT);
			// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			Log.i(TAG, "Got viewFactory: " + viewFactory);
			// Register our Activity so Dynamix can close us later on request from the IAndroidFacade
			TestHarness.registerConfigurationActivity(runtime, TestConfigurationHostActivity.this);
			handler.post(new Runnable() {
			    public void run() {
				/*
				 * Note that, as soon as this method is called, the View code is injected into the Main
				 * UI thread, which makes it possible for Views to crash Dynamix. From what I can tell,
				 * there is really no way to protect ourselves from this, other than deactivating the
				 * plug-in and restarting. Ideas: - Use a second process and a secured AIDL channel for
				 * running plug GUIs (and even runtimes)
				 */
				setContentView(v, new ViewGroup.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.FILL_PARENT));
			    }
			});
		    }
		    else
			Log.w(TAG, "IContextPluginViewFactory was NULL");
		}
		catch (Exception e) {
		    Log.e(TAG, "ContextPluginConfigurationHostActivity: " + e.toString());
		}
	    }
	    else
		Log.e(TAG, "Could not find ContextPluginRuntime for: " + pluginId);
	}
    };

    @Override
    protected void onDestroy() {
	super.onDestroy();
	if (viewFactory != null)
	    try {
		viewFactory.destroyView();
	    }
	    catch (Exception e) {
		Log.w(TAG, "Exception when destroying view: " + e);
	    }
	viewFactory = null;
	
    }
}
