/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugin.dev;

import java.lang.reflect.Field;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.ambientdynamix.api.contextplugin.ContextInfoSet;
import org.ambientdynamix.api.contextplugin.ContextPlugin;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.IContextPluginConfigurationViewFactory;
import org.ambientdynamix.api.contextplugin.IContextPluginInteractionViewFactory;
import org.ambientdynamix.api.contextplugin.IContextPluginRuntimeFactory;
import org.ambientdynamix.api.contextplugin.IPluginContextListener;
import org.ambientdynamix.api.contextplugin.IPluginEventHandler;
import org.ambientdynamix.api.contextplugin.IPluginFacade;
import org.ambientdynamix.api.contextplugin.NfcListener;
import org.ambientdynamix.api.contextplugin.PluginAlert;
import org.ambientdynamix.api.contextplugin.PluginState;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.ReactiveContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.security.SecuredContext;
import org.ambientdynamix.api.contextplugin.security.SecuredContextInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

/**
 * TestHarness is a simple Dynamix environment used for testing and debugging Context Plugins. Running plugins within
 * the TestHarness allows developers to debug without needing to deploy a plug-in within the Dyanmix Framework.
 * 
 * @author Darren Carlson
 */
public class TestHarness implements IPluginContextListener, IPluginFacade {
	private static final String TAG = TestHarness.class.getSimpleName();
	// Private data
	private static ContextPlugin plug;
	private static ContextPluginRuntime runtime;
	private static UUID sessionId;
	private static TestConfigurationHostActivity configActivity;
	private static TestAcquisitionHostActivity acquisitionActivity;
	private IContextPluginRuntimeFactory runtimeFactory;
	private IContextPluginInteractionViewFactory acquisitionFactory;
	private IContextPluginConfigurationViewFactory configurationFactory;
	private IPluginEventHandler handler;
	private SecuredContext sContext;
	private ContextPluginSettings settings;
	private PowerScheme scheme;
	private List<UUID> requests = new Vector<UUID>();
	private PluginState plugState;
	private Activity context;
	private List<NfcListener> nfcListeners = new Vector<NfcListener>();
	private Handler mainThreadHandler = new Handler();

	public static void registerConfigurationActivity(ContextPluginRuntime runtime2,
			TestConfigurationHostActivity configActivity) {
		TestHarness.configActivity = configActivity;
	}

	public static void setAcquisitionActivity(TestAcquisitionHostActivity acquisitionActivity) {
		TestHarness.acquisitionActivity = acquisitionActivity;
	}

	public void initTestHarness(ContextPlugin plug, IContextPluginRuntimeFactory runtimeFactory, PowerScheme scheme,
			Activity context) throws Exception {
		initTestHarness(plug, runtimeFactory, null, scheme, context);
	}

	public void initTestHarness(ContextPlugin plug, IContextPluginRuntimeFactory runtimeFactory, PowerScheme scheme,
			ContextPluginSettings settings, Activity context) throws Exception {
		initTestHarness(plug, runtimeFactory, settings, scheme, context);
	}

	public void initTestHarness(ContextPlugin plug, IContextPluginRuntimeFactory runtimeFactory,
			ContextPluginSettings settings, PowerScheme scheme, Activity context) throws Exception {
		acceptAllSelfSignedSSLcertificates();
		Log.i(TAG, "initTestHarness for: " + plug);
		if (TestHarness.plug != null) {
			Log.w(TAG, "initTestHarness is destroying existing plugin: " + TestHarness.plug);
			destroyPlugin();
		}
		// Setup initial variable state
		TestHarness.plug = plug;
		this.context = context;
		this.runtimeFactory = runtimeFactory;
		this.settings = settings;
		this.scheme = scheme;
		this.sContext = new SecuredContext(context, mainThreadHandler, Looper.myLooper(), context.getClass()
				.getClassLoader(), true);
		this.plugState = PluginState.NEW;
		sessionId = UUID.randomUUID();
		if (runtimeFactory instanceof org.ambientdynamix.api.contextplugin.ContextPluginRuntimeFactory) {
			// Create the new ContextPluginRuntime using the factory
			ContextPluginRuntime runtime = runtimeFactory.makeContextPluginRuntime(plug, this,
					new SimpleEventHandler(), sessionId);
			/*
			 * Initialize the ContextPluginRuntime using the event handler, power scheme and settings.
			 */
			if (runtime != null) {
				// Initialize the runtime
				runtime.init(scheme, settings);
				// Add the runtime to the TestHarness
				TestHarness.runtime = runtime;
				// Add the TestHarness as a context listener (Remember, we can only add listeners after INIT!)
				runtime.addContextListener(this);
				// Set INITIALIZED plug state
				plugState = PluginState.INITIALIZED;
				Log.i(TAG, "Runtime is initialized for: " + plug);
			} else
				throw new Exception("Factory failed to create runtime!");
		} else
			throw new Exception(
					"Factory not an instanceof org.ambientdynamix.api.contextplugin.ContextPluginRuntimeFactory");
	}

	public void launchConfigActivity() {
		if (runtime.getSettingsViewFactory() != null) {
			Intent i = new Intent(context, TestConfigurationHostActivity.class);
			i.putExtra("pluginId", plug.getId());
			context.startActivity(i);
		} else
			Log.w(TAG, "No configuration factory for: " + plug);
	}

	public void startPlugin() {
		Log.i(TAG, "startPlugin for: " + plug);
		if (runtime != null) {
			if (plugState == PluginState.INITIALIZED) {
				if (plug.requiresConfiguration() && !plug.isConfigured()) {
					Log.w(TAG, "Cannot start. Plug-in requires configuration");
				} else {
					Thread t = new Thread(new Runnable() {
						@Override
						public void run() {
							Log.i(TAG, "Starting runtime on Thread: " + Thread.currentThread());
							try {
								plugState = PluginState.STARTED;
								runtime.start();
								Log.i(TAG, "Exiting start method");
							} catch (Exception e) {
								Log.e(TAG, "Start exception: " + e);
								plugState = PluginState.ERROR;
							}
						}
					});
					t.setDaemon(true);
					t.start();
				}
			} else
				Log.w(TAG, "Cannot start from state: " + plugState);
		} else
			Log.w(TAG, "Runtime is null!");
	}

	public void stopPlugin() {
		Log.i(TAG, "stopPlugin for: " + plug);
		if (runtime != null) {
			if (plugState == PluginState.STARTED) {
				try {
					runtime.stop();
					plugState = PluginState.INITIALIZED;
				} catch (Exception e) {
					Log.e(TAG, "Stop exception: " + e);
					plugState = PluginState.ERROR;
				}
			} else
				Log.w(TAG, "Cannot stop from state: " + plugState);
		} else
			Log.w(TAG, "Runtime is null!");
	}

	public void destroyPlugin() {
		Log.i(TAG, "destroyPlugin for: " + plug);
		if (runtime != null) {
			try {
				runtime.destroy();
				plugState = PluginState.DESTROYED;
			} catch (Exception e) {
				Log.e(TAG, "Destroy exception: " + e);
				plugState = PluginState.ERROR;
			}
		} else
			Log.w(TAG, "Runtime is null!");
	}

	public void setPowerScheme(PowerScheme scheme) {
		Log.i(TAG, "setPowerScheme for: " + plug + " | with scheme: " + scheme);
		this.scheme = scheme;
		if (runtime != null) {
			try {
				runtime.setPowerScheme(scheme);
			} catch (Exception e) {
				Log.e(TAG, "Set power scheme exception: " + e);
			}
		} else
			Log.w(TAG, "Runtime is null!");
	}

	public void updateSettings(ContextPluginSettings settings) {
		Log.i(TAG, "updateSettings for: " + plug);
		this.settings = settings;
		if (runtime != null) {
			try {
				runtime.updateSettings(settings);
			} catch (Exception e) {
				Log.e(TAG, "Update settings exception: " + e);
			}
		} else
			Log.w(TAG, "Runtime is null!");
	}

	public void handleContextRequest(final String contextInfoType) {
		Log.i(TAG, "handleContextRequest for: " + plug + " | Context Type: " + contextInfoType);
		if (plugState == PluginState.STARTED) {
			if (runtime instanceof ReactiveContextPluginRuntime) {
				final ReactiveContextPluginRuntime tmp = (ReactiveContextPluginRuntime) runtime;
				final UUID requestId = UUID.randomUUID();
				requests.add(requestId);
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						Log.i(TAG, "handleContextRequest on dedicated Thread");
						try {
							tmp.handleContextRequest(requestId, contextInfoType);
						} catch (Exception e) {
							Log.w(TAG, "Error during context scan: " + e.toString());
						}
					}
				});
				t.setDaemon(true);
				t.start();
			}
		} else
			Log.w(TAG, "Cannot handleContextRequest from state: " + plugState);
	}

	public void handleConfiguredContextRequest(final String contextInfoType, final Bundle scanConfig) {
		Log.i(TAG, "handleConfiguredContextRequest for: " + plug + " | Context Type: " + contextInfoType);
		if (plugState == PluginState.STARTED) {
			if (runtime instanceof ReactiveContextPluginRuntime) {
				final ReactiveContextPluginRuntime tmp = (ReactiveContextPluginRuntime) runtime;
				final UUID requestId = UUID.randomUUID();
				requests.add(requestId);
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						Log.i(TAG, "handleConfiguredContextRequest on dedicated Thread");
						try {
							tmp.handleConfiguredContextRequest(requestId, contextInfoType, scanConfig);
						} catch (Exception e) {
							Log.w(TAG, "Error during context scan: " + e.toString());
						}
					}
				});
				t.setDaemon(true);
				t.start();
			}
		} else
			Log.w(TAG, "Cannot handleContextRequest from state: " + plugState);
	}

	@Override
	public void onContextRequestFailed(UUID sessionId, UUID requestId, String errorMessage, int errorCode) {
		if (requests.remove(requestId))
			Log.i(TAG, "onContextScanFailed: sessionId = " + sessionId + " | requestId = " + requestId
					+ " | errorMessage = " + errorMessage);
		else
			Log.w(TAG, "requestId not found for onContextScanFailed");
	}

	@Override
	public void onPluginContextEvent(UUID sessionId, ContextInfoSet dataSet) {
		Log.i(TAG, "onPluginContextEvent: sessionId = " + sessionId);
		Log.i(TAG, "========= Begin Context Event  =========");
		Log.i(TAG, "Event Type: " + dataSet.getEventType());
		Log.i(TAG, "Event Timestamp: " + dataSet.getTimeStamp());
		if (dataSet.expires())
			Log.i(TAG, "Event expires in: " + dataSet.getExpireMills() + " milliseconds");
		else
			Log.i(TAG, "Event does not expire");
		Log.i(TAG, "Embedded Context Info count: " + dataSet.getSecuredContextInfo().size());
		for (SecuredContextInfo info : dataSet.getSecuredContextInfo()) {
			Log.i(TAG, "----------- Context Info -----------");
			Log.i(TAG, "--- ContextType: " + info.getContextInfo().getContextType());
			Log.i(TAG, "--- ImplementingClassname: " + info.getContextInfo().getImplementingClassname());
			Log.i(TAG, "--- PrivacyRisk: " + info.getPrivacyRisk());
			for (String format : info.getContextInfo().getStringRepresentationFormats()) {
				Log.i(TAG, "--- StringRepresentationFormat: " + format);
				Log.i(TAG, "--- StringRepresentation: " + info.getContextInfo().getStringRepresentation(format));
			}
		}
		Log.i(TAG, "========= End Context Event  =========");
	}

	@Override
	public ContextPluginSettings getContextPluginSettings(UUID sessionID) {
		Log.i(TAG, "getContextPluginSettings: sessionId = " + sessionID);
		if (sessionId.equals(sessionID)) {
			return settings;
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
		return null;
	}

	@Override
	public SecuredContext getSecuredContext(UUID sessionID) {
		Log.i(TAG, "getSecuredContext: sessionId = " + sessionID);
		if (sessionId.equals(sessionID)) {
			return sContext;
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
		return null;
	}

	@Override
	public boolean setPluginConfiguredStatus(UUID sessionID, boolean configured) {
		Log.i(TAG, "setPluginConfiguredStatus: sessionId = " + sessionID + " | configured = " + configured);
		if (sessionId.equals(sessionID)) {
			TestHarness.plug.setConfigured(configured);
			return true;
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
		return false;
	}

	@Override
	public boolean storeContextPluginSettings(UUID sessionID, ContextPluginSettings settings) {
		Log.i(TAG, "storeContextPluginSettings: sessionId = " + sessionID + " | settings = " + settings);
		if (sessionId.equals(sessionID)) {
			this.settings = settings;
			return true;
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
		return false;
	}

	@Override
	public PluginState getState(UUID sessionID) {
		Log.i(TAG, "getState: sessionId = " + sessionID);
		if (sessionId.equals(sessionID)) {
			return plugState;
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
		return null;
	}

	public static ContextPlugin getContextPlugin(String pluginId) {
		Log.i(TAG, "getContextPlugin: pluginId = " + pluginId);
		if (plug.getId() != null && plug.getId().equals(pluginId)) {
			return plug;
		} else
			Log.w(TAG, "Plugin Id mismatch: " + pluginId);
		return null;
	}

	public static ContextPluginRuntime getContextPluginRuntime() {
		return runtime;
	}

	@Override
	public void closeContextAcquisitionView(UUID sessionID) {
		Log.i(TAG, "closeContextAcquisitionView: sessionId = " + sessionID);
		if (sessionId.equals(sessionID)) {
			if (acquisitionActivity != null) {
				acquisitionActivity.finish();
				acquisitionActivity = null;
			}
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
	}

	@Override
	public void closeConfigurationView(UUID sessionID) {
		Log.i(TAG, "closeConfigurationView for: " + configActivity);
		if (sessionId.equals(sessionID)) {
			if (configActivity != null) {
				configActivity.finish();
				configActivity = null;
			}
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
	}

	public static void acceptAllSelfSignedSSLcertificates() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };
		HostnameVerifier hv = new HostnameVerifier() {
			@Override
			public boolean verify(String urlHostName, SSLSession session) {
				System.out.println("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
				return true;
			}
		};
		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS"); // Note that we need
			// TLS on Android
			// (not SSL)
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (Exception e) {
			// Log.e(TAG, "SSLContext Error: " + e.toString());
		}
	}

	@Override
	public boolean addNfcListener(UUID sessionID, NfcListener listener) {
		if (sessionId.equals(sessionID)) {
			if (!nfcListeners.contains(listener))
				return nfcListeners.add(listener);
			else
				Log.w(TAG, "NfcListener already registered: " + listener);
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
		return false;
	}

	@Override
	public boolean removeNfcListener(UUID sessionID, NfcListener listener) {
		if (sessionId.equals(sessionID)) {
			if (nfcListeners.contains(listener))
				return nfcListeners.remove(listener);
			else
				Log.w(TAG, "Could not find NfcListener: " + listener);
		} else
			Log.w(TAG, "Missing or incorrect sessionId: " + sessionID);
		return false;
	}

	@Override
	public boolean sendPluginAlert(UUID sessionID, PluginAlert alert) {
		Toast.makeText(context, alert.getAlertMessage(), 10000);
		if (alert.getRequestedSettingsActivity() != null) {
			String requestedActivity = alert.getRequestedSettingsActivity();
			try {
				Field f = Settings.class.getDeclaredField(requestedActivity);
				Log.i(TAG, "Opening Settings Activity Intent: " + requestedActivity);
				context.startActivity(new Intent(requestedActivity));
				return true;
			} catch (Exception e) {
				Log.w(TAG, "Plug-in requested an illegal Settings Activity: " + requestedActivity);
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean cancelContextRequestId(UUID sessionId, UUID requestId) {
		return requests.remove(requestId);
	}
}