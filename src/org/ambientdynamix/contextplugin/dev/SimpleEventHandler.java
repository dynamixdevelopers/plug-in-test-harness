/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugin.dev;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

import org.ambientdynamix.api.contextplugin.*;
import org.ambientdynamix.api.contextplugin.security.*;

/**
 * Simple implementation of the IPluginEventHandler interface. Uses a thread-safe Vector for maintaining the list of
 * listeners. Automatically creates a snapshot of the current listener list before sending events.
 * 
 * @author Darren Carlson
 */
public class SimpleEventHandler implements IPluginEventHandler {
	// Private data
	private List<IPluginContextListener> listeners = new Vector<IPluginContextListener>();

	/**
	 * Adds a IPluginContextListener if it has not already been added
	 */
	public void addContextListener(IPluginContextListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	/**
	 * Removes a previously added IPluginContextListener
	 */
	public void removeContextListener(IPluginContextListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Sends the specified SecuredEvents to registered listeners using the specified ContextPluginRuntime as the event
	 * sender.
	 */
	@Override
	public void sendEvent(ContextPluginRuntime sender, ContextInfoSet infoSet) {
		List<IPluginContextListener> snapshot = new Vector<IPluginContextListener>(listeners);
		for (IPluginContextListener l : snapshot) {
			l.onPluginContextEvent(sender.getSessionId(), infoSet);
		}
		snapshot.clear();
		snapshot = null;
	}

	@Override
	public void sendError(ContextPluginRuntime sender, UUID requestId, String errorMessage, int errorCode) {
		List<IPluginContextListener> snapshot = new Vector<IPluginContextListener>(listeners);
		for (IPluginContextListener l : snapshot) {
			l.onContextRequestFailed(sender.getSessionId(), requestId, errorMessage, errorCode);
		}
		snapshot.clear();
		snapshot = null;
	}
}