/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugin.dev;

import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.ambientdynamix.api.contextplugin.ContextPlugin;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IContextPluginInteractionViewFactory;
import org.ambientdynamix.api.contextplugin.InteractiveContextPluginRuntime;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

/**
 * An Android Activity configured to host a dynamically injected context acquisition user interface provided by an
 * authorized ContextPluginRuntime. Dynamix applications wishing to launch a particular ContextPlugin's acquisition
 * interface must send a specially constructed Android intent, using the following method:
 * <ol>
 * <li>Create an intent with the intent filter: 'org.ambientdynamix.contextplugin.ACQUIRE_CONTEXT'</li>
 * <li>Use intent.putExtra with the key 'pluginId' and the value of the plugin id of the requested ContextPlugin</li>
 * <li>Use intent.putExtra with the key 'sessionId' and the value of the session id of the application</li>
 * </ol>
 * This class first checks that the specified ContextPlugin has permission to launch its configuration interface. Next,
 * this class uses the incoming sessionId to check that the application has permission to launch the specified
 * ContextPlugin interface before allowing the Activity to inject the interface.
 * <p>
 * Note: Application permission checking is not fully complete. At the moment, we allow all applications to launch any
 * ContextPlugin with a context acquisition user interface.
 * 
 * @author Darren Carlson
 */
public class TestAcquisitionHostActivity extends Activity {
	private final String TAG = this.getClass().getSimpleName();
	public static int TITLEBAR_HEIGHT;
	// Private data
	private ContextPluginRuntime wrapper;
	private IContextPluginInteractionViewFactory viewFactory;
	private UUID requestId = null;
	private Map<String, Boolean> requestMap = new Hashtable<String, Boolean>();

	@Override
	protected void onNewIntent(Intent intent) {
		Log.v(TAG, "Activity State: onNewIntent()");
		setIntent(intent);
	};

	@Override
	public void onResume() {
		super.onResume();
		Log.v(TAG, "Activity State: onResume()");
		// Grab the intent and extras
		Intent i = getIntent();
		Bundle extras = i.getExtras();
		String pluginId = extras.getString("pluginId");
		TestHarness.setAcquisitionActivity(this);
		// Create an UUID using the sessionId string
		if (pluginId != null) {
			ContextPlugin plug = TestHarness.getContextPlugin(pluginId);
			wrapper = TestHarness.getContextPluginRuntime();
			Log.d(TAG, "getContextPluginRuntime for pluginId: " + pluginId);
			if (plug.requiresConfiguration() && !plug.isConfigured()) {
				Log.w(TAG, "Plugin Not Configured: " + pluginId);
			} else {
				// Make sure its an InteractiveContextPluginRuntime
				if (wrapper instanceof InteractiveContextPluginRuntime) {
					final InteractiveContextPluginRuntime runtime = (InteractiveContextPluginRuntime) wrapper;
					Handler handler = new Handler();
					handler.post(new Runnable() {
						public void run() {
							try {
								// Dynamically create the AcquisitionViewFactory
								Class<IContextPluginInteractionViewFactory> factory = runtime
										.getAcquisitionViewFactory();
								viewFactory = factory.newInstance();
								// Create the View using the factory
								View v = viewFactory.initializeView(
										runtime.getPluginFacade().getSecuredContext(runtime.getSessionId()), runtime,
										requestId, "contextType", TITLEBAR_HEIGHT);
								// Inject the plugin's View into the host Activity
								setContentView(v, new ViewGroup.LayoutParams(LayoutParams.FILL_PARENT,
										LayoutParams.FILL_PARENT));
								// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
								// Return so finish is not called
								return;
							} catch (Exception e) {
								Log.e(TAG, e.toString());
							}
						}
					});
				} else {
					Log.w(TAG, "Not a ReactiveContextPluginRuntime for pluginId: " + pluginId);
				}
			}
		} else {
			Log.w(TAG, "Null pluginId string!");
		}
		this.finish();
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (viewFactory != null)
			viewFactory.destroyView();
		viewFactory = null;
		TestHarness.setAcquisitionActivity(null);
	}
}